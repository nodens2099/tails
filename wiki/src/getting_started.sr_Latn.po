# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"POT-Creation-Date: 2018-07-28 08:14+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Plain text
#, no-wrap
msgid "[[!meta title=\"Getting started…\"]]\n"
msgstr ""

#. type: Title -
#, no-wrap
msgid "Is Tails the right tool for me?\n"
msgstr ""

#. type: Bullet: '1. '
msgid "First, read the [[about|about]] page to get a basic idea of what Tails is."
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"2. Then read the [[warning|doc/about/warning]] page to understand **what "
"Tails doesn't protect you\n"
"against**, how you can screw up your anonymity or leave traces.\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"3. If Tails is the right tool for you, go on [[downloading, verifying, and\n"
"installing it|install]].\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid ""
"Learning more about Tails…\n"
"--------------------------\n"
msgstr ""

#. type: Plain text
msgid ""
"- Read our [[user documentation|doc]] to learn more about Tails and how to "
"use it."
msgstr ""

#. type: Bullet: '- '
msgid ""
"Learn more about Tor, the anonymity network used by Tails on their website: "
"[[https://www.torproject.org/]]."
msgstr ""

#. type: Bullet: '- '
msgid ""
"For a deeper understanding of Tails' specifications and internals, read our "
"[[design document|contribute/design]]."
msgstr ""

#. type: Title -
#, no-wrap
msgid "Found a bug?\n"
msgstr ""

#. type: Plain text
msgid ""
"If you found a bug in Tails or want to request a new feature, see our "
"[[support]] pages."
msgstr ""

#. type: Title -
#, no-wrap
msgid "Get in touch with us\n"
msgstr ""

#. type: Plain text
#, no-wrap
msgid "[[!inline pages=\"support/talk\" raw=\"yes\" sort=\"age\"]]\n"
msgstr ""
