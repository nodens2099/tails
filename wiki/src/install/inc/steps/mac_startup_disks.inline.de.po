# SOME DESCRIPTIVE TITLE
# Copyright (C) YEAR Free Software Foundation, Inc.
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: 2019-10-18 01:54+0000\n"
"PO-Revision-Date: 2016-05-08 10:42+0200\n"
"Last-Translator: Tails translators <tails@boum.org>\n"
"Language-Team: \n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.7.1\n"

#. type: Content of: <ol><li><p>
msgid "Shut down the computer while leaving the USB stick plugged in."
msgstr ""

#. type: Content of: <ol><li><p>
msgid "Plug in the other Tails USB stick that you want to install from."
msgstr ""

#. type: Content of: <ol><li><p>
#, fuzzy
#| msgid ""
#| "   <p class=\"mac mac-clone\">Immediately press-and-hold the\n"
#| "   <span class=\"keycap\">Option</span> key (<span class=\"keycap\">Alt</"
#| "span> key) until a list of possible startup\n"
#| "   disks appears.</p>\n"
msgid ""
"Switch on the computer and immediately press-and-hold the <span class="
"\"keycap\">Option</span> key (<span class=\"keycap\">Alt</span> key) until a "
"list of possible startup disks appears."
msgstr ""
"   <p class=\"mac mac-clone\">Drücken und halten Sie sofort die Taste\n"
"   <span class=\"keycap\">Option</span> (<span class=\"keycap\">Alt</span>-"
"Taste), bis eine Liste der\n"
"   Speichermedien, von denen gestartet werden kann, erscheint.</p>\n"

#. type: Content of: <ol><li><p>
#, fuzzy
#| msgid ""
#| "   [[!img install/inc/screenshots/mac_option_key.png class=\"mac mac-clone"
#| "\" link=\"no\"]]\n"
msgid ""
"[[!img install/inc/screenshots/mac_option_key.png link=\"no\" alt=\"'Option' "
"or 'alt' key in the bottom left of Mac keyboard\"]]"
msgstr ""
"   [[!img install/inc/screenshots/mac_option_key.png class=\"mac mac-clone\" "
"link=\"no\"]]\n"

#. type: Content of: <ol><li><p>
#, fuzzy
#| msgid ""
#| "   <p class=\"mac mac-clone\">Choose the USB stick and press\n"
#| "   <span class=\"keycap\">Enter</span>. The USB stick appears as an "
#| "external hard\n"
#| "   disk and might be labeled <span class=\"guilabel\">EFI Boot</span> or\n"
#| "   <span class=\"guilabel\">Windows</span> like in the following "
#| "screenshot:</p>\n"
msgid ""
"Choose the USB stick and press <span class=\"keycap\">Enter</span>. The USB "
"stick appears as an external hard disk and might be labeled <span class="
"\"guilabel\">EFI Boot</span> or <span class=\"guilabel\">Windows</span> like "
"in the following screenshot:"
msgstr ""
"   <p class=\"mac mac-clone\">Wählen Sie den USB-Stick aus und drücken Sie\n"
"   <span class=\"keycap\">Enter</span>. Der USB-Stick erscheint als externe "
"Festplatte\n"
"   und heißt möglicherweise <span class=\"guilabel\">EFI Boot</span>\n"
"   oder <span class=\"guilabel\">Windows</span>, wie im folgenden "
"Bildschirmfoto:</p>\n"

#. type: Content of: <ol><li><p>
#, fuzzy
#| msgid ""
#| "   [[!img install/inc/screenshots/mac_startup_usb.png class=\"mac mac-"
#| "clone\" link=\"no\" alt=\"Screen with the logo of an internal hard disk "
#| "labeled 'Macintosh HD' and an external hard disk labelled "
#| "'Windows' (selected)\"]]\n"
msgid ""
"[[!img install/inc/screenshots/mac_startup_usb.png link=\"no\" alt=\"Screen "
"with the logo of an internal hard disk labeled 'Macintosh HD' and an "
"external hard disk labelled 'Windows' (selected)\"]]"
msgstr ""
"   [[!img install/inc/screenshots/mac_startup_usb.png class=\"mac mac-clone"
"\" link=\"no\" alt=\"Bildschirm mit dem Logo einer internen Festplatte mit "
"der Bezeichnung 'Macintosh HD' und einer externen Festplatte mit der\n"
"  Bezeichnung 'Windows' (ausgewählt)\"]]\n"

#. type: Content of: <ol><li><div><p>
msgid "If the USB stick does not appear in the list of possible startup disks:"
msgstr ""

#. type: Content of: <ol><li><div><ol><li>
msgid ""
"Make sure that you have [[verified your download of Tails|install/mac/usb-"
"download]]."
msgstr ""

#. type: Content of: <ol><li><div><ol><li>
msgid "Try installing again on the same USB stick."
msgstr ""

#. type: Content of: <ol><li><div><ol><li>
msgid "Try installing on a different USB stick."
msgstr ""

#. type: Content of: <ol><li><div><ol><li>
msgid "Try using the same USB stick to start on a different computer."
msgstr ""

#. type: Content of: <ol><li><div><ol><li>
msgid ""
"If the same USB stick works on a different computer, please [[!toggle id="
"\"report-toggle-1\" text=\"report the problem to our help desk\"]]."
msgstr ""

#. type: Content of: <ol>
msgid ""
"[[!toggleable id=\"report-toggle-1\" text=\"\"\" <span class=\"hide\">[[!"
"toggle id=\"report-toggle-1\" text=\"\"]]</span> [[!inline pages=\"install/"
"inc/steps/reporting.inline\" raw=\"yes\"]] \"\"\"]]"
msgstr ""

#. type: Content of: <ol><li><p>
msgid ""
"If the computer starts on Tails, the <span class=\"application\">Boot Loader "
"Menu</span> appears and Tails starts automatically after 4 seconds."
msgstr ""

#. type: Content of: <ol><li><p>
msgid ""
"[[!img install/inc/screenshots/boot_loader_menu.png link=\"no\" alt=\"Black "
"screen with Tails artwork. Boot Loader Menu with two options 'Tails' and "
"'Tails (Troubleshooting Mode)'.\"]]"
msgstr ""

#. type: Content of: <div><p>
msgid ""
"If your computer displays the <span class=\"application\">Boot Loader Menu</"
"span> but then fails to start on the USB stick, it might currently be "
"impossible to start Tails on your Mac model."
msgstr ""

#. type: Content of: <div><p>
msgid ""
"Please [[!toggle id=\"report-toggle\" text=\"report the problem to our help "
"desk\"]]."
msgstr ""

#. type: Content of: outside any tag (error?)
msgid ""
"[[!toggleable id=\"report-toggle\" text=\"\"\" <span class=\"hide\">[[!"
"toggle id=\"report-toggle\" text=\"\"]]</span> [[!inline pages=\"install/inc/"
"steps/reporting.inline\" raw=\"yes\"]] \"\"\"]]"
msgstr ""
