<h2>Make the computer start on the USB stick</h2>

<p>This step is the most complicated one as it depends a lot on the
model of the computer:</p>

<ul>
<li>On most computers, our basic instructions to get to the boot menu
work fine.</li>
<li>On some computers, you might have to do more advanced techniques,
like editing the BIOS settings.</li>
<li>On a few computers, it might currently be impossible to start Tails.
But, it is very hard to know without trying.</li>
</ul>

<p>The goal is to reach the <span class="application">Boot Loader
Menu</span> of Tails:</p>

<p>[[!img install/inc/screenshots/boot_loader_menu.png link="no" alt="Black screen with Tails artwork. Boot Loader Menu with two options 'Tails' and 'Tails (Troubleshooting Mode)'."]]</p>

<div class="next">

<p>After you reach the <span class="application">Boot Loader Menu</span>, you
can skip the rest of this section and <a href="#greeter">wait until
<span class="application">Tails Greeter</span> appears</a>.</p>

</div>

<h3 id="boot-menu">Getting to the boot menu</h3>

<p>On most computers, you can press a <em>boot menu key</em> to display
a list of possible devices to start from. The following instructions
explain how to display the boot menu and start on the USB stick. The
following screenshot is an example of a boot menu:</p>

<p>[[!img install/inc/screenshots/bios_boot_menu.png link="no" alt=""]]</p>

<ol>

<li>
  <p class="usb upgrade-windows">Shut down the computer while leaving the USB stick plugged in.</p>
  <p class="clone upgrade-tails">Shut down the computer.</p>
  <p class="clone">Plug in the other Tails USB stick that you want to
  <span class="install-clone">install</span>
  <span class="upgrade">upgrade</span>
  from.</p>
  <p class="upgrade-tails">Unplug your Tails USB stick while leaving the intermediary USB stick plugged in.</p>
</li>

<li>
  <p>Identify the possible boot menu keys for the computer depending on
  the computer manufacturer in the following list:</p>

  <table>
    <tr><th>Manufacturer</th><th>Key</th></tr>
    <tr><td>Acer</td><td>Esc, F12, F9</td></tr>
    <tr><td>Asus</td><td>Esc, F8</td></tr>
    <tr><td>Clevo</td><td>F7</td></tr>
    <tr><td>Dell</td><td>F12</td></tr>
    <tr><td>Fujitsu</td><td>F12, Esc</td></tr>
    <tr><td>HP</td><td>F9, Esc</td></tr>
    <tr><td>Lenovo</td><td>F12, Novo, F8, F10</td></tr>
    <tr><td>Samsung</td><td>Esc, F12, F2</td></tr>
    <tr><td>Sony</td><td>F11, Esc, F10</td></tr>
    <tr><td>Toshiba</td><td>F12</td></tr>
    <tr><td>others&hellip;</td><td>F12, Esc</td></tr>
  </table>

  <div class="tip">
  <p>On many computers, a message is displayed very briefly when switching on
  that also explains how to get to the boot menu or edit the BIOS settings.</p>
  </div>
</li>

<li>
  <p>Switch on the computer and immediately press several times the first
  possible boot menu key identified in step 2.</p>
</li>

<li>
  <p>If the computer starts on another operating system or returns an
  error message, shut down the computer again and repeat step
  3 for all the possible boot menu keys identified in step 2.</p>

  <p>If a boot menu with a list of devices appears, select your USB stick
  and press <span class="keycap">Enter</span>.</p>

  <div class="note">
  <p>If the boot menu key that works on your computer is a different one than
  the first in the list, please let us know so we can improve the list. You can write to
  [[sajolida@pimienta.org]] (private email).</p>
  </div>
</li>

</ol>

<p id="animation">This animation explains how to use the boot menu key:</p>

<video controls="true" width="640" height="360" poster="https://tails.boum.org/install/inc/videos/boot-menu-key.png">
  <source src="https://tails.boum.org/install/inc/videos/boot-menu-key.webm" type="video/webm" />
  <source src="https://tails.boum.org/install/inc/videos/boot-menu-key.mp4" type="video/mp4" />
</video>

<h3>Check our list of known issues</h3>

<p>Similar problems might have been reported already for your model of computer
in our [[list of known issues|support/known_issues]].</p>

<h3 id="bios-settings">Edit the BIOS settings</h3>

<p>If none of the possible boot menu keys from the previous technique work, or if
the USB stick does not appear in the boot menu, you might need to edit your BIOS
settings.</p>

<div class="tip">
<p>Search for the user manual of the computer on the website of its
manufacturer to learn how to edit the BIOS settings:</p>
<ul>
  <li><a href="http://us.acer.com/ac/en/US/content/drivers">Acer</a></li>
  <li><a href="https://www.asus.com/us/support/">Asus</a></li>
  <li><a href="https://www.dell.com/support/home/us/en/19/Products/">Dell</a></li>
  <li><a href="https://www.fujitsu.com/global/support/products/software/manual/">Fujitsu</a></li>
  <li><a href="https://support.hp.com/us-en/products/">HP</a></li>
  <li><a href="http://support.lenovo.com/us/en/">Lenovo</a></li>
  <li><a href="http://www.samsung.com/us/support/downloads">Samsung</a></li>
  <li><a href="http://esupport.sony.com/US">Sony</a></li>
  <li><a href="http://support.toshiba.com/support/products?cat=Computers">Toshiba</a></li>
</ul>
</div>

<p>In the BIOS settings, try to apply the following changes one by one and
restart the computer after each change. Some changes might not
apply to certain computer models.</p>

<div class="caution">
<p>Take note of the changes that you apply to the BIOS settings. That way, you
can revert the changes if they prevent the computer from starting on its usual
operating system.</p>
</div>

<ol>
  <li>
   <p>Edit the <strong>Boot Order</strong>. Depending on the computer model you might
   see an entry for <strong>removable devices</strong> or <strong>USB media</strong>. Move this entry
   to the top of the list to force the computer to try to start from
   the USB stick before starting from the internal hard disk.</p>
  </li>

  <li>
    <p>Disable <strong>Fast boot</strong>.</p>
  </li>

  <li>
    <p>If the computer is configured to start with <strong>legacy BIOS</strong>, try to
   configure it to start with <strong>UEFI</strong>. Else, if the computer is configured
   to start with <strong>UEFI</strong>, try to configure it to start with <strong>legacy
   BIOS</strong>. To do so, try any of the following options if available:</p>

    <ul>
      <li>Enable <strong>Legacy mode</strong></li>
      <li>Disable <strong>Secure boot</strong>
         (see the <a href="https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/disabling-secure-boot">Microsoft documentation</a>)</li>
      <li>Enable <strong>CSM boot</strong></li>
      <li>Disable <strong>UEFI</strong></li>
    </ul>
   </li>

   <li><p>Try to upgrade the BIOS to the latest version provided by the manufacturer.</p></li>
</ol>

<div class="bug" id="report">
<p>If none of these options work, we are sorry but you might not be
able to use Tails on this computer. Feel free to
[[!toggle id="report-toggle-2" text="report the problem to our help desk"]].</p>
</div>

[[!toggleable id="report-toggle-2" text="""
<span class="hide">[[!toggle id="report-toggle-2" text=""]]</span>

[[!inline pages="install/inc/steps/reporting.inline" raw="yes"]]

"""]]
