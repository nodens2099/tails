[[!meta title="Delete the persistent volume"]]

<div class="caution">

<p>This technique is fast but might not prevent a strong attacker from
recovering files from the old persistent volume using advanced [[data
recovery techniques|encryption_and_privacy/secure_deletion#why]].</p>

<p>To securely delete the persistent volume, you have to [[securely
delete the entire USB stick|encryption_and_privacy/secure_deletion#erase-device]],
which is a much slower operation.</p>

</div>

  1. Start Tails from the USB stick on which you want to delete the
     persistent volume.

     Do not enable the persistent volume in <span class="application">Tails Greeter</span>.

  1. Choose
     <span class="menuchoice">
     <span class="guimenu">Applications</span>&nbsp;▸
     <span class="guisubmenu">Tails</span>&nbsp;▸
     <span class="guimenuitem">Delete persistent volume</span></span>.

  1. Click <span class="guilabel">Delete</span>.
